#!/usr/bin/env bash

_ago_cli_completion() {
  local word="${COMP_WORDS[COMP_CWORD]}"
  local previous_word="${COMP_WORDS[COMP_CWORD - 1]}"
  local _2ndPrevious_word="${COMP_WORDS[COMP_CWORD - 2]}"
  if [ "$COMP_CWORD" -eq 1 ]; then
    # shellcheck disable=SC2207
    COMPREPLY=($(compgen -W "$(ago commands)" -- "$word"))
    return 0
  else
    case "$previous_word" in
    add)
      case "$_2ndPrevious_word" in
      remote)
        # shellcheck disable=SC2207
        COMPREPLY=($(compgen -A hostname "${word}"))
        return 0
        ;;
      esac
      # shellcheck disable=SC2207
      COMPREPLY=($(compgen -f -X '!*.@(py|out)' "${word}"))
      return 0
      ;;
    rm)
      case "$_2ndPrevious_word" in
      remote)
        completions=$(ago subset "remote_rm")
        # shellcheck disable=SC2207
        COMPREPLY=($(compgen -W "$completions" -- "$word"))
        return 0
        ;;
      esac
      ;;
    esac
    local words=("${COMP_WORDS[@]}")
    unset 'words[0]'
    unset 'words[$COMP_CWORD]'
    local completions
    completions=$(ago subset "${words[@]}")
    # shellcheck disable=SC2207
    COMPREPLY=($(compgen -W "$completions" -- "$word"))
    return 0
  fi
}

complete -F _ago_cli_completion ago
