package main

import (
	"agoSystem/interface/commands"
	"fmt"
	"os"
)

func main() {
	if len(os.Args) == 1 {
		_, _ = commands.Perform("help")
		return
	}
	if len(os.Args) > 1 {
		command := os.Args[1]
		if status, err := commands.Perform(command); err != nil {
			fmt.Println(err)
		} else if status == commands.CommandNotFound {
			_, _ = commands.Perform("help")
		}
	}
}
