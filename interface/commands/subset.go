package commands

import (
	"fmt"
	"os"
)

func init() {
	subsetCmd := Command{
		Action: func() error {
			if len(os.Args) == 3 {
				command := os.Args[2]
				if commands != nil &&
					commands[command] != nil &&
					commands[command].SubSet != nil {
					for _, w := range commands[command].SubSet() {
						fmt.Println(w)
					}
				}
			}
			return nil
		},
		Name:      "subset",
		Signature: "",
		SubSet:    nil,
		Usage:     nil,
	}
	subsetCmd.registerCommand()
}
