package commands

import (
	"agoSystem/kernel/config"
	"crypto/sha1"
	"errors"
	"fmt"
	"os"
	"text/tabwriter"
	"time"
)

func init() {
	remoteCmd := Command{
		Action: func() error {
			if len(os.Args) < 3 {
				return errors.New("action required")
			}
			action := os.Args[2]
			switch action {
			case "add":
				if len(os.Args) < 4 {
					return errors.New("location required")
				}
				switch config.Status(config.RemoteConfig) {
				case config.ConfFileNotFound:
					return errors.New("ago's not configured yet. use \"ago config init\"")
				}
				location := os.Args[3]
				locationHash := sha1.Sum([]byte(location + time.Now().String()))
				config.NewKey("remotes", "name", fmt.Sprintf("%x", locationHash), config.RemoteConfig)
				config.NewKey(fmt.Sprintf("remote %x", locationHash), "location", location, config.RemoteConfig)
			case "ls":
				status, remotes := config.FetchAll("remotes", "name", config.RemoteConfig)
				if status != config.Ok {
					return nil
				}
				if len(remotes) >= 1 {
					w := new(tabwriter.Writer)
					w.Init(os.Stdout, 8, 8, 0, '\t', 0)
					_, _ = fmt.Fprintf(w, "\n %s\t%s\t", "  Remote  ", "  Location  ")
					_, _ = fmt.Fprintf(w, "\n %s\t%s\t", "----------", "------------")
					_ = w.Flush()
					for _, remote := range remotes {
						_, location := config.Fetch(fmt.Sprintf("remote %s", remote), "location", config.RemoteConfig)
						if remote == "" {
							continue
						}
						remote := remote
						_, _ = fmt.Fprintf(w, "\n  %s\t %s\t", remote[:8], location)
						_ = w.Flush()
					}
					fmt.Printf("\n\n")
				}
			case "rm":
				if len(os.Args) < 4 {
					remove := func(remote string) {
						config.DeleteOne("remotes", "name", remote, config.RemoteConfig)
						config.DeleteSection(fmt.Sprintf("remote %s", remote), config.RemoteConfig)
					}
					removeAll := func() {
						_, remotes := config.FetchAll("remotes", "name", config.RemoteConfig)
						for _, remote := range remotes {
							config.Delete("remotes", "name", config.RemoteConfig)
							config.DeleteSection(fmt.Sprintf("remote %s", remote), config.RemoteConfig)
						}
					}
					_, remotes := config.FetchAll("remotes", "name", config.RemoteConfig)
					for _, remote := range remotes {
						if remote == "" {
							continue
						}
						fmt.Printf("Remove %s? [Y/n/a/q] ", remote[:8])
						var res string
						n, _ := fmt.Scanln(&res)
						if n == 0 {
							continue
						}
						switch res {
						case "n":
							continue
						case "a":
							removeAll()
							return nil
						case "q":
							return nil
						case "y":
							remove(remote)
						case "Y":
							remove(remote)
						default:
							continue
						}
					}
					return nil
				}
				hash := os.Args[3]
				status, remotes := config.FetchAll("remotes", "name", config.RemoteConfig)
				if status != config.Ok {
					return nil
				}
				var matches []string
				for _, remote := range remotes {
					if remote[:len(hash)] == hash {
						matches = append(matches, remote)
					}
				}
				if len(matches) > 1 {
					for _, remote := range matches {
						fmt.Println(remote)
					}
				} else if len(matches) == 1 {
					config.DeleteOne("remotes", "name", matches[0], config.RemoteConfig)
					config.DeleteSection(fmt.Sprintf("remote %s", matches[0]), config.RemoteConfig)
				}
			case "e":
				modify := func(remote, location string) {
					config.Set(fmt.Sprintf("remote %s", remote), "location", location, config.RemoteConfig)
				}
				if len(os.Args) < 4 {
					_, remotes := config.FetchAll("remotes", "name", config.RemoteConfig)
					for _, remote := range remotes {
						if remote != "" {
							_, location := config.Fetch(fmt.Sprintf("remote %s", remote), "location", config.RemoteConfig)
							fmt.Printf("%s: %s ? [K/M]", remote[:8], location)
							var res string
							_, _ = fmt.Scanln(&res)
							if res == "m" || res == "M" {
								fmt.Printf("%s Location: ", remote[:8])
								var location string
								_, _ = fmt.Scanln(&location)
								if location != "" {
									modify(remote, location)
								}
							}
						}
					}
					return nil
				}
				hash := os.Args[3]
				status, remotes := config.FetchAll("remotes", "name", config.RemoteConfig)
				if status != config.Ok {
					return nil
				}
				var matches []string
				for _, remote := range remotes {
					if remote[:len(hash)] == hash {
						matches = append(matches, remote)
					}
				}
				if len(matches) > 1 {
					for _, remote := range matches {
						fmt.Println(remote)
					}
				} else if len(matches) == 1 {
					_, location := config.Fetch(fmt.Sprintf("remote %s", matches[0]), "location", config.RemoteConfig)
					fmt.Printf("%s: %s ? [K/M]", matches[0][:8], location)
					var res string
					_, _ = fmt.Scanln(&res)
					if res == "m" || res == "M" {
						fmt.Printf("%s Location: ", matches[0][:8])
						var location string
						_, _ = fmt.Scanln(&location)
						if location != "" {
							modify(matches[0], location)
						}
					}
				}
			}
			return nil
		},
		Name:      "remote",
		Signature: "remote <action>",
		SubSet: func() []string {
			return []string{
				"add",
				"ls",
				"rm",
				"e",
			}
		},
		Usage: map[string]string{
			"remote ls":               "list added ago system's name and location.",
			"remote add <location>":   "add location (uri:port) as an ago system.",
			"remote rm [remote hash]": "remove ago system information.",
			"remote e [remote hash]":  "edit ago system information.",
		},
	}
	remoteCmd.registerCommand()
}
