package commands

import (
	"agoSystem/kernel/ago"
	"agoSystem/kernel/agolog"
	"errors"
	"fmt"
	"io/ioutil"
	"os"
)

func init() {
	rmCmd := Command{
		Action: func() error {
			_, err := os.Stat(ago.BaseDir)
			if os.IsNotExist(err) {
				return errors.New("Not an ago based directory.\nUse \"ago init\" to initialize ago")
			}
			_, err = ioutil.ReadDir(ago.ProgramDir)
			if err != nil {
				agolog.Log(err.Error())
				return os.Mkdir(ago.ProgramDir, 0775)
			}
			if len(os.Args) < 3 {
				return errors.New("program hash required")
			}
			programHash := os.Args[2]
			programPath := fmt.Sprintf("%s/%s/", ago.ProgramDir, programHash[:2])
			programs, err := ago.FindProgram(programHash)
			if err != nil {
				agolog.Log(err.Error())
				return nil
			}
			if programs != nil {
				if len(programs) > 1 {
					for _, program := range programs {
						fmt.Println(program.Name())
						return nil
					}
				} else if len(programs) == 1 {
					if err := os.Remove(fmt.Sprintf("%s/%s", programPath, programs[0].Name())); err != nil {
						agolog.Log(err.Error())
						return nil
					}
				}
			}
			return nil
		},
		Name:      "rm",
		Signature: "rm <program hash>",
		SubSet:     findPrograms,
		Usage:       map[string]string{
			"rm <program hash>": "remove ago program object.",
		},
	}
	rmCmd.registerCommand()
}
