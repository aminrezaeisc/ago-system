package commands
//
//import (
//	"errors"
//	"fmt"
//	"golang.org/x/crypto/ssh/terminal"
//	"os"
//	"os/exec"
//	"os/user"
//	"syscall"
//)
//
//func init() {
//	serviceCmd := Command{
//		Action: func() error {
//			if len(os.Args) < 3 {
//				return errors.New("action required")
//			}
//			action := os.Args[2]
//			switch action {
//			case "start":
//				if len(os.Args) == 4 {
//					service := os.Args[3]
//					switch service {
//					case "rago":
//						if syscall.Getuid() == 0 {
//							cmd := fmt.Sprintf("systemctl start rago.service")
//							_, err := exec.Command("bash", "-c", cmd).Output()
//							if err != nil {
//								fmt.Printf("Failed to start service: %v\n", err)
//							}
//						} else {
//							usr, _ := user.Current()
//							fmt.Printf("[sudo] password for %s: ", usr.Username)
//							passwd, _ := terminal.ReadPassword(0)
//							cmd := fmt.Sprintf("echo %s | sudo -S systemctl start rago.service", passwd)
//							_, err := exec.Command("bash", "-c", cmd).Output()
//							if err != nil {
//								fmt.Printf("Failed to execute command: %s\n", cmd)
//							}
//							fmt.Println()
//						}
//					case "reago":
//					}
//				} else if len(os.Args) == 3 {
//					if syscall.Getuid() == 0 {
//						cmd := fmt.Sprintf("systemctl start rago.service")
//						// TODO -> cmd := fmt.Sprintf("systemctl start reago.service")
//						_, err := exec.Command("bash", "-c", cmd).Output()
//						if err != nil {
//							fmt.Printf("Failed to start service: %v\n", err)
//						}
//					} else {
//						usr, _ := user.Current()
//						fmt.Printf("[sudo] password for %s: ", usr.Username)
//						passwd, _ := terminal.ReadPassword(0)
//						cmd := fmt.Sprintf("echo %s | sudo -S systemctl start rago.service", passwd)
//						// TODO -> cmd := fmt.Sprintf("echo %s | sudo -S systemctl start reago.service", passwd)
//						_, err := exec.Command("bash", "-c", cmd).Output()
//						if err != nil {
//							fmt.Printf("Failed to execute command: %s\n", cmd)
//						}
//						fmt.Println()
//					}
//				}
//			case "stop":
//			case "status":
//			case "restart":
//			case "ls":
//				fmt.Println(" agoR: ago system discovery service")
//				fmt.Println("agoRE: ago system remote execution service")
//			}
//			return nil
//		},
//		Name:      "service",
//		Signature: "service <action>",
//		SubSet: func() []string {
//			return []string{
//				"start",
//				"status",
//				"stop",
//				"restart",
//				"ls",
//			}
//
//		},
//		Usage: map[string]string{
//			"service ls":                "list ago system's services.",
//			"service start [service]":   "start service(s).",
//			"service stop [service]":    "stop service(s).",
//			"service restart [service]": "restart service(s).",
//			"service status [service]":  "print service's status on the standard output.",
//		},
//	}
//	serviceCmd.registerCommand()
//}
