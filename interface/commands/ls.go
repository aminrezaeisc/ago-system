package commands

import (
	"agoSystem/kernel/ago"
	"agoSystem/kernel/agolog"
	"errors"
	"fmt"
	"io/ioutil"
	"os"
)

func init() {
	lsCmd := Command{
		Action: func() error {
			_, err := os.Stat(ago.BaseDir)
			if os.IsNotExist(err) {
				return errors.New("Not an ago based directory.\nUse \"ago init\" to initialize ago")
			}
			dirs, err := ioutil.ReadDir(ago.ProgramDir)
			if err != nil {
				return os.Mkdir(ago.ProgramDir, 0775)
			}
			for _, dir := range dirs {
				files, err := ioutil.ReadDir(fmt.Sprintf("%s/%s", ago.ProgramDir, dir.Name()))
				if err != nil {
					agolog.Log(err.Error())
					return nil
				}
				for _, file := range files {
					if file.Name()[:1] != "." {
						if !file.IsDir() {
							fmt.Println(file.Name()[:8])
						}
					}
				}
			}
			return nil
		},
		Name:      "ls",
		Signature: "ls",
		SubSet:    nil,
		Usage: map[string]string{
			"ls": "list ago programs in current directory" +
				" and print on the standard output.",
		},
	}
	lsCmd.registerCommand()
}
