package commands

import (
	"agoSystem/kernel/ago"
	"agoSystem/kernel/agolog"
	"agoSystem/kernel/config"
	rago "agoSystem/service/agoR/client"
	reago "agoSystem/service/agoRE/client"
	"errors"
	"fmt"
	"io/ioutil"
	"os"
)

func init() {
	runCmd := Command{
		Action: func() error {
			_, err := os.Stat(ago.BaseDir)
			if os.IsNotExist(err) {
				return errors.New("Not an ago based directory.\nUse \"ago init\" to initialize ago")
			}
			_, err = ioutil.ReadDir(ago.ProgramDir)
			if err != nil {
				agolog.Log(err.Error())
				return os.Mkdir(ago.ProgramDir, 0775)
			}
			if len(os.Args) < 3 {
				return errors.New("program hash required")
			}
			programHash := os.Args[2]
			hash := "local"
			if len(os.Args) == 4 {
				hash = os.Args[3]
			}
			agoProgram := ago.Program{
				Name: programHash,
			}
			status, err := agoProgram.ReadText()
			if err != nil {
				agolog.Log(err.Error())
				return nil
			}
			switch status {
			case ago.Ok:
				if hash == "local" {
					_, err := agoProgram.Run()
					if err != nil {
						agolog.Log(err.Error())
						return nil
					}
					_, err = agoProgram.Save()
					if err != nil {
						agolog.Log(err.Error())
						return nil
					}
					return nil
				} else {
					status, remotes := config.FetchAll("remotes", "name", config.RemoteConfig)
					if status != config.Ok {
						return nil
					}
					var matches []string
					for _, remote := range remotes {
						if remote[:len(hash)] == hash {
							matches = append(matches, remote)
						}
					}
					if len(matches) > 1 {
						for _, remote := range matches {
							fmt.Println(remote)
						}
						return nil
					} else if len(matches) == 1 {
						remote := matches[0]
						status, location := config.Fetch(fmt.Sprintf("remote %s", remote), "location", config.RemoteConfig)
						if status != config.Ok {
							return errors.New(fmt.Sprintf("could not fetch location of %s\n", remote))
						}
						service, err := rago.Discover(location)
						if err != nil {
							agolog.Log(err.Error())
							return nil
						}
						if !service.SagoRE {
							return errors.New(fmt.Sprintf("remote %s's RE service has not activated yet\n", remote))
						}
						if service.SagoRELocation != "" {
							reago.RemoteRun(&agoProgram, service.SagoRELocation)
							_, err := agoProgram.Save()
							if err != nil {
								agolog.Log(err.Error())
								return nil
							}
						} else {
							return errors.New(fmt.Sprintf("remote %s did not specified its RE service location\n", remote))
						}
					}
				}
			case ago.ProgramSignatureNotValid:
				return errors.New(fmt.Sprintf("%s is not an ago program", agoProgram.Name))
			}
			return nil
		},
		Name:      "run",
		Signature: "run <program hash> [remote hash]",
		SubSet:    findPrograms,
		Usage: map[string]string{
			"run <program hash> [remote hash]": "run program on remote if specified.",
		},
	}
	runCmd.registerCommand()
}
