package commands

import (
	"agoSystem/kernel/config"
)

func init() {
	rmCmd := Command{
		Action: func() error {
			return nil
		},
		Name:      "remote_rm",
		Signature: "",
		SubSet: func() []string {
			_, remotes := config.FetchAll("remotes", "name", config.RemoteConfig)
			return remotes
		},
		Usage:       map[string]string{},
	}
	rmCmd.registerCommand()
}
