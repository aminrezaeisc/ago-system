package commands

import (
	"agoSystem/kernel/ago"
	"agoSystem/kernel/agolog"
	"errors"
	"fmt"
	"io/ioutil"
	"os"
)

func init() {
	errCmd := Command{
		Action: func() error {
			_, err := os.Stat(ago.BaseDir)
			if os.IsNotExist(err) {
				return errors.New("Not an ago based directory.\nUse \"ago init\" to initialize ago")
			}
			_, err = ioutil.ReadDir(ago.ProgramDir)
			if err != nil {
				agolog.Log(err.Error())
				return os.Mkdir(ago.ProgramDir, 0775)
			}
			if len(os.Args) < 3 {
				return errors.New("program hash required")
			}
			programHash := os.Args[2]
			agoProgram := ago.Program{
				Name: programHash,
			}
			status, err := agoProgram.ReadStdErr()
			if err != nil {
				agolog.Log(err.Error())
				return nil
			}
			switch status {
			case ago.Ok:
				fmt.Println(string(agoProgram.StdErr))
			case ago.ProgramSignatureNotValid:
				return errors.New(fmt.Sprintf("%s is not an ago program", agoProgram.Name))
			}
			return nil
		},
		Name:      "err",
		Signature: "err <program hash>",
		SubSet:    findPrograms,
		Usage: map[string]string{
			"err <program hash>": "print errors of program, if the program was executed, on the standard output.",
		},
	}
	errCmd.registerCommand()
}
