package commands

import (
	"agoSystem/kernel/agolog"
	"agoSystem/kernel/config"
	"agoSystem/service/agoR/client"
	"errors"
	"fmt"
	"os"
	"text/tabwriter"
)

func init() {
	nwCmd := Command{
		Action: func() error {
			switch config.Status(config.RemoteConfig) {
			case config.ConfFileNotFound:
				return errors.New("ago's not configured yet. use \"ago config init\"")
			}
			status, remotes := config.FetchAll("remotes", "name", config.RemoteConfig)
			if status != config.Ok {
				return nil
			}
			if len(remotes) >= 1 {
				w := new(tabwriter.Writer)
				w.Init(os.Stdout, 8, 8, 0, '\t', 0)
				_, _ = fmt.Fprintf(w, "\n %s\t%s\t%s\t", "  Remote  ", "  R  ", "  R.E  ")
				_, _ = fmt.Fprintf(w, "\n %s\t%s\t%s\t", "----------", "-----", "-------")
				_ = w.Flush()
				for _, remote := range remotes {
					if remote == "" {
						continue
					}
					_, location := config.Fetch(fmt.Sprintf("remote %s", remote), "location", config.RemoteConfig)
					_, _ = fmt.Fprintf(w, "\n  %s\t  ", remote[:8])
					_ = w.Flush()
					service, err := client.Discover(location)
					if err != nil {
						agolog.Log(err.Error())
						_, _ = fmt.Fprintf(w, "%s\t   %s\t", "✗", "✗")
						_ = w.Flush()
						continue
					}
					R := "✗"
					RE := "✗"
					if service.SagoR == true {
						R = "✔"
					}
					if service.SagoRE == true {
						RE = "✔"
					}
					_, _ = fmt.Fprintf(w, "%s\t   %s\t", R, RE)
					_ = w.Flush()
				}
				fmt.Printf("\n\n")
			}
			return nil
		},
		Name:      "network",
		Signature: "network",
		SubSet:    nil,
		Usage: map[string]string{
			"network": "check remotes connectivity",
		},
	}
	nwCmd.registerCommand()
}
