package commands

import (
	"fmt"
	"os"
	"path"
)

func init() {
	helpCmd := Command{
		Action: func() error {
			if len(os.Args) < 3 {
				binaryName := fmt.Sprintf(path.Base(os.Args[0]))
				fmt.Printf("%s usage:\n", binaryName)
				for _, command := range commands {
					if command.Signature != "" {
						fmt.Printf("\t%s %s\n", binaryName, command.Signature)
					}
				}
			} else {
				command := os.Args[2]
				Help(command)
			}
			return nil
		},
		Name:      "help",
		Signature: "help [command]",
		SubSet: func() (subset []string) {
			for command, cmd := range commands {
				if cmd.Signature != "" {
					subset = append(subset, command)
				}
			}
			return subset
		},
		Usage: map[string]string{
			"help":           "list ago cli primary commands and print on the standard output.",
			"help <command>": "print help message specific to given command on the standard output.",
		},
	}
	helpCmd.registerCommand()
}
