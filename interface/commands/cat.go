package commands

import (
	"agoSystem/kernel/ago"
	"errors"
	"fmt"
	"io/ioutil"
	"os"
)

func init() {
	catCmd := Command{
		Action: func() error {
			_, err := os.Stat(ago.BaseDir)
			if os.IsNotExist(err) {
				return errors.New("Not an ago based directory.\nUse \"ago init\" to initialize ago")
			}
			_, err = ioutil.ReadDir(ago.ProgramDir)
			if err != nil {
				return os.Mkdir(ago.ProgramDir, 0775)
			}
			if len(os.Args) < 3 {
				return errors.New("program hash required")
			}
			programHash := os.Args[2]
			agoProgram := ago.Program{
				Name: programHash,
			}
			status, err := agoProgram.ReadText()
			if err != nil {
				return err
			}
			switch status {
			case ago.Ok:
				fmt.Println(string(agoProgram.Text))
			case ago.ProgramSignatureNotValid:
				return errors.New(fmt.Sprintf("%s is not an ago program", agoProgram.Name))
			}
			return nil
		},
		Name:      "cat",
		Signature: "cat <program hash>",
		SubSet:    findPrograms,
		Usage: map[string]string{
			"cat <program hash>": "print program itself on the standard output.",
		},
	}
	catCmd.registerCommand()
}
