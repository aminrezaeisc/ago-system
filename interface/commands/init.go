package commands

import (
	"agoSystem/kernel/ago"
	"os"
)

func init() {
	initCmd := Command{
		Action: func() error {
			_, err := os.Stat(ago.BaseDir)
			if os.IsNotExist(err) {
				return os.Mkdir(ago.BaseDir, 0775)
			}
			return nil
		},
		Name:      "init",
		Signature: "init",
		SubSet:    nil,
		Usage:       map[string]string{
			"init": "initialize current directory as ago directory.",
		},
	}
	initCmd.registerCommand()
}
