package commands

func init() {
	commandsCmd := Command{
		Action: func() error {
			ListCommands()
			return nil
		},
		Name:      "commands",
		Signature: "",
		SubSet:    nil,
		Usage:     nil,
	}
	commandsCmd.registerCommand()
}
