package commands

import (
	"agoSystem/kernel/ago"
	"fmt"
	"io/ioutil"
	"os"
)

const (
	Ok = 0x0
	CommandNotFound = 0x01
)

var commands map[string]*Command

type CommandStatus int

type Command struct {
	Action    func() error
	Name      string
	Signature string
	SubSet    func() []string
	Usage     map[string]string
}

// registerCommand registers command object.
func (command *Command) registerCommand() {
	if commands == nil {
		commands = make(map[string]*Command)
	}
	commands[command.Name] = command
}

// ListCommands lists all the primary ago commands and prints on the standard output.
func ListCommands() {
	for command, cmd := range commands {
		if cmd.Signature != "" {
			fmt.Println(command)
		}
	}
}

// Help prints given command usage on the standard output.
func Help(command string) {
	if commands != nil && commands[command] != nil {
		for cmd, usage := range commands[command].Usage {
			fmt.Printf("%s:\n\t%s\n", cmd, usage)
		}
	}
}

// Perform performs given command action if there is any.
func Perform(command string) (CommandStatus, error) {
	if commands[command] != nil {
		return Ok, commands[command].Action()
	}
	return CommandNotFound, nil
}

// findPrograms looks for ago program objects in current directory.
func findPrograms() (subset []string) {
	_, err := os.Stat(".ago")
	if os.IsNotExist(err) {
		return nil
	}
	dirs, err := ioutil.ReadDir(ago.ProgramDir)
	if err != nil {
		_ = os.Mkdir(ago.ProgramDir, 0775)
		return nil
	}
	for _, dir := range dirs {
		files, err := ioutil.ReadDir(fmt.Sprintf("%s/%s", ago.ProgramDir, dir.Name()))
		if err != nil {
			return nil
		}
		for _, file := range files {
			if file.Name()[:1] != "." && !file.IsDir() {
				subset = append(subset, file.Name())
			}
		}
	}
	return subset
}
