package commands

import (
	"agoSystem/kernel/ago"
	"agoSystem/kernel/agolog"
	"crypto/sha1"
	"errors"
	"fmt"
	"io/ioutil"
	"os"
	"time"
)

func init() {
	addCmd := Command{
		Action: func() error {
			_, err := os.Stat(ago.BaseDir)
			if os.IsNotExist(err) {
				return errors.New("Not an ago based directory.\nUse \"ago init\" to initialize ago")
			}
			if len(os.Args) < 3 {
				return errors.New("A program required to be added")
			}
			programPath := os.Args[2]
			program, err := ioutil.ReadFile(programPath)
			if err != nil {
				agolog.Log(err.Error())
				return nil
			}
			if len(program) == 0 {
				return errors.New(fmt.Sprintf("%s is empty", programPath))
			}
			programHash := sha1.Sum([]byte(string(program) + time.Now().String()))
			agoProgramPath := fmt.Sprintf("%s/%x", ago.ProgramDir, programHash[0])
			_, err = os.Stat(agoProgramPath)
			if os.IsNotExist(err) {
				if err := os.MkdirAll(agoProgramPath, 0775); err != nil {
					agolog.Log(err.Error())
					return nil
				}
			}
			_, err = os.Stat(fmt.Sprintf("%s/%x", agoProgramPath, programHash))
			if os.IsNotExist(err) {
				agoProgram := ago.Program{
					Name: fmt.Sprintf("%x", programHash),
					Text: program,
				}
				agoProgramObject, status := agoProgram.Object()
				if status == ago.Ok {
					err = ioutil.WriteFile(fmt.Sprintf("%s/%x", agoProgramPath, programHash), agoProgramObject, 0600)
					if err != nil {
						agolog.Log(err.Error())
						return nil
					}
					fmt.Println(programHash)
				} else if status == ago.ProgramTextEmpty {
					// not possible, already checked the program file to not be empty
					return errors.New(fmt.Sprintf("%s is empty", programPath))
				}
			}
			return nil
		},
		Name:      "add",
		Signature: "add <executable|script>",
		SubSet:    nil,
		Usage: map[string]string{
			"add <executable|script>": "add specified executable or script" +
				" to ago system to be executed later either locally or remotely.",
		},
	}
	addCmd.registerCommand()
}