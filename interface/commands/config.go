package commands

import (
	"agoSystem/kernel/config"
	"errors"
	"fmt"
	"os"
	"syscall"
)

func init() {
	confInitCmd := Command{
		Action: func() error {
			if len(os.Args) < 3 {
				return errors.New("action required")
			}
			switch os.Args[2] {
			case "init":
				if config.Status(config.GlobalConfig) != config.Ok ||
					config.Status(config.RemoteConfig) != config.Ok ||
					config.Status(config.ServiceConfig) != config.Ok {
					if config.Init() != config.Ok {
						return errors.New("could not configure ago")
					}
				}
			case "service":
				switch config.Status(config.ServiceConfig) {
				case config.ConfFileNotFound:
					return errors.New("ago's not configured yet. use \"ago config init\"")
				}
				if len(os.Args) == 5 {
					if syscall.Getuid() == 0 {
						switch os.Args[4] {
						case "on":
							config.Set(fmt.Sprintf("service %s", os.Args[3]), "status", "on", config.ServiceConfig)
						case "off":
							config.Set(fmt.Sprintf("service %s", os.Args[3]), "status", "off", config.ServiceConfig)
						}
					}
					return nil
				}
				_, agoRLocation := config.Fetch("service agoR", "location", config.ServiceConfig)
				_, agoRELocation := config.Fetch("service agoRE", "location", config.ServiceConfig)
				if agoRLocation != "" {
					fmt.Printf("R: %s ? [K/M]", agoRLocation)
					var res string
					_, _ = fmt.Scanln(&res)
					if res == "m" || res == "M" {
						fmt.Print("R Location: ")
						var location string
						_, _ = fmt.Scanln(&location)
						if location != "" {
							config.Set("service agoR", "location", location, config.ServiceConfig)
						}
					}
				} else {
					fmt.Print("R Location: ")
					var location string
					_, _ = fmt.Scanln(&location)
					if location != "" {
						config.Set("service agoR", "location", location, config.ServiceConfig)
					}
				}
				if agoRELocation != "" {
					fmt.Printf("RE: %s ? [K/M]", agoRELocation)
					var res string
					_, _ = fmt.Scanln(&res)
					if res == "m" || res == "M" {
						fmt.Print("RE Location: ")
						var location string
						_, _ = fmt.Scanln(&location)
						if location != "" {
							config.Set("service agoRE", "location", location, config.ServiceConfig)
						}
					}
				} else {
					fmt.Print("RE Location: ")
					var location string
					_, _ = fmt.Scanln(&location)
					if location != "" {
						config.Set("service agoRE", "location", location, config.ServiceConfig)
					}
				}
			}
			return nil
		},
		Name:
		"config",
		Signature: "config <action>",
		SubSet: func() []string {
			return []string{
				"init",
				"service",
			}
		},
		Usage: map[string]string{
			"config init":    "auto configure ago for current user",
			"config service": "configure ago services for current user",
		},
	}
	confInitCmd.registerCommand()
}
