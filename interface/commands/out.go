package commands

import (
	"agoSystem/kernel/ago"
	"agoSystem/kernel/agolog"
	"errors"
	"fmt"
	"io/ioutil"
	"os"
)

func init() {
	resultCmd := Command{
		Action: func() error {
			_, err := os.Stat(ago.BaseDir)
			if os.IsNotExist(err) {
				return errors.New("Not an ago based directory.\nUse \"ago init\" to initialize ago")
			}
			_, err = ioutil.ReadDir(ago.ProgramDir)
			if err != nil {
				agolog.Log(err.Error())
				return os.Mkdir(ago.ProgramDir, 0775)
			}
			if len(os.Args) < 3 {
				return errors.New("program hash required")
			}
			programHash := os.Args[2]
			agoProgram := ago.Program{
				Name: programHash,
			}
			status, err := agoProgram.ReadStdOut()
			if err != nil {
				agolog.Log(err.Error())
				return nil
			}
			switch status {
			case ago.Ok:
				fmt.Println(string(agoProgram.StdOut))
			case ago.ProgramSignatureNotValid:
				return errors.New(fmt.Sprintf("%s is not an ago program", agoProgram.Name))
			}
			return nil
		},
		Name:      "out",
		Signature: "out <program hash>",
		SubSet:     findPrograms,
		Usage: map[string]string{
			"out <program hash>": "print result of program, if the program was executed, on the standard output.",
		},
	}
	resultCmd.registerCommand()
}
