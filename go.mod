module agoSystem

go 1.13

replace agoSystem => ./

require (
	github.com/golang/protobuf v1.4.2
	github.com/smartystreets/goconvey v1.6.4 // indirect
	golang.org/x/crypto v0.0.0-20190308221718-c2843e01d9a2
	google.golang.org/grpc v1.29.1
	google.golang.org/protobuf v1.24.0
	gopkg.in/ini.v1 v1.57.0
)
