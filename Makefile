build:
	go build -gcflags '-N' -o bin/ago interface/ago.go
	go build -o bin/rago service/agoR/server/agoRServer.go
	go build -o bin/reago service/agoRE/server/agoREServer.go

proto:
	protoc --go_out=plugins=grpc:service/agoR/ protocol/agoR/agoR.proto
	protoc --go_out=plugins=grpc:service/agoRE/ protocol/agoRE/agoRE.proto

configure:
	mkdir -p ${HOME}/ago/bin/
	install bin/ago ${HOME}/ago/bin/
	install bin/rago ${HOME}/ago/bin/
	install bin/reago ${HOME}/ago/bin/
	chmod u+s ${HOME}/ago/bin/ago
	chmod u+s ${HOME}/ago/bin/rago
	chmod u+s ${HOME}/ago/bin/reago

daemon:
	bash service/agoR/daemonize.sh
	bash service/agoRE/daemonize.sh

install:
	cp interface/ago_completion.sh /etc/bash_completion.d/
	cp service/agoR/rago.service /etc/systemd/system/
	cp service/agoRE/reago.service /etc/systemd/system/
