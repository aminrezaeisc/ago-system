package agoNet

import (
	"errors"
	"fmt"
	"net"
)

func TcpListener(address string) (net.Listener, error) {
	listener, err := net.Listen("tcp", address)
	if err != nil {
		return nil, errors.New(fmt.Sprintf("failed to listen on address %s: %v\n", address, err))
	}
	return listener, nil
}
