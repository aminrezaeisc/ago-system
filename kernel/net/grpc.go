package agoNet

import "google.golang.org/grpc"

func GrpcClientConnection(location string) (*grpc.ClientConn, error) {
	connection, err := grpc.Dial(location, grpc.WithInsecure())
	if err != nil {
		return nil, err
	}
	return connection, nil
}