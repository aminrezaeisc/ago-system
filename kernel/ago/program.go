package ago

import (
	"bytes"
	"encoding/binary"
	"errors"
	"fmt"
	"io/ioutil"
	"os"
	"os/exec"
)

type ProgramStatus byte
type ProgramPortion byte
type RunType byte

type Program struct {
	Name   string
	Text   []byte
	StdOut []byte
	StdErr []byte
}

// Object turns program's text, stdout and stderr into ago program object.
func (program *Program) Object() (agop []byte, status ProgramStatus) {
	if len(program.Text) == 0 {
		return agop, ProgramTextEmpty
	} else {
		// ago program signature
		agop = append(agop, 0xAC, 0x04)
		agop = append(agop, "AGOSYSPO"...)
		agop = append(agop, 0x0)

		// ago program text
		programLen := make([]byte, 4)
		binary.LittleEndian.PutUint32(programLen, uint32(len(program.Text)))
		agop = append(agop, programLen...)
		agop = append(agop, 0x0)
		agop = append(agop, program.Text...)
		agop = append(agop, 0x0)
		// ago program stdout
		resultLen := make([]byte, 4)
		binary.LittleEndian.PutUint32(resultLen, uint32(len(program.StdOut)))
		agop = append(agop, resultLen...)
		agop = append(agop, 0x0)
		if len(program.StdOut) != 0 {
			agop = append(agop, program.StdOut...)
		}
		agop = append(agop, 0x0)
		// ago program stderr
		errLen := make([]byte, 4)
		binary.LittleEndian.PutUint32(errLen, uint32(len(program.StdErr)))
		agop = append(agop, errLen...)
		agop = append(agop, 0x0)
		if len(program.StdErr) != 0 {
			agop = append(agop, program.StdErr...)
		}
		agop = append(agop, 0x0)
		return agop, Ok
	}
}

// ReadText loads ago program object from current directory and
// reads text portion of object and store it to program's text
func (program *Program) ReadText() (status ProgramStatus, err error) {
	programs, err := FindProgram(program.Name)
	if err != nil {
		return Ok, err
	}
	if programs != nil {
		if len(programs) < 1 {
			for _, program := range programs {
				fmt.Println(program.Name())
				return Ok, nil
			}
		} else if len(programs) == 1 {
			program.Name = programs[0].Name()
			data, err := ioutil.ReadFile(fmt.Sprintf("%s/%s/%s", ProgramDir, programs[0].Name()[:2], programs[0].Name()))
			if err != nil {
				return Ok, err
			}
			status, text := readData(data, Text)
			if status == Ok {
				program.Text = text
				return Ok, nil
			} else {
				return status, nil
			}
		}
	}
	return Ok, nil
}

// ReadStdOut loads ago program object from current directory and
// reads stdout portion of object and store it to program's stdout
func (program *Program) ReadStdOut() (status ProgramStatus, err error) {
	programs, err := FindProgram(program.Name)
	if err != nil {
		return Ok, err
	}
	if programs != nil {
		if len(programs) < 1 {
			for _, program := range programs {
				fmt.Println(program.Name())
				return Ok, nil
			}
		} else if len(programs) == 1 {
			data, err := ioutil.ReadFile(fmt.Sprintf("%s/%s/%s", ProgramDir, programs[0].Name()[:2], programs[0].Name()))
			if err != nil {
				return Ok, err
			}
			status, out := readData(data, StdOut)
			if status == Ok {
				program.StdOut = out
				return Ok, nil
			} else {
				return status, nil
			}
		}
	}
	return Ok, nil
}

// ReadStdErr loads ago program object from current directory and
// reads stderr portion of object and store it to program's stderr
func (program *Program) ReadStdErr() (status ProgramStatus, err error) {
	programs, err := FindProgram(program.Name)
	if err != nil {
		return Ok, err
	}
	if programs != nil {
		if len(programs) < 1 {
			for _, program := range programs {
				fmt.Println(program.Name())
				return Ok, nil
			}
		} else if len(programs) == 1 {
			data, err := ioutil.ReadFile(fmt.Sprintf("%s/%s/%s", ProgramDir, programs[0].Name()[:2], programs[0].Name()))
			if err != nil {
				return Ok, err
			}
			status, e := readData(data, StdErr)
			if status == Ok {
				program.StdErr = e
				return Ok, nil
			} else {
				return status, nil
			}
		}
	}
	return Ok, nil
}

// Run runs program text locally and store its standard output and standard error internally
func (program *Program) Run() (ProgramStatus, error) {
	pythonFile := fmt.Sprintf("/tmp/.%s.py", program.Name)
	if program.Text == nil || len(program.Text) == 0 {
		return ProgramTextEmpty, nil
	}
	if err := ioutil.WriteFile(pythonFile, program.Text, 0700); err != nil {
		return Err, errors.New(fmt.Sprintf("could not run %s: %v", program.Name, err))
	}
	exe := exec.Command(pythonFile)
	stdOut, err := exe.Output()
	var stdErr []byte
	if err != nil {
		stdErr = []byte(err.Error())
	} else {
		stdErr = []byte("no error")
	}
	program.StdOut = stdOut
	program.StdErr = stdErr
	return Ok, nil
}

// Save saves program object at current directory
func (program *Program) Save() (ProgramStatus, error) {
	agoProgramObject, status := program.Object()
	if status == Ok {
		agoProgramPath := fmt.Sprintf("%s/%s", ProgramDir, program.Name[:2])
		_, err := os.Stat(agoProgramPath)
		if os.IsNotExist(err) {
			if err := os.MkdirAll(agoProgramPath, 0775); err != nil {
				return Err, err
			}
		}
		err = ioutil.WriteFile(fmt.Sprintf("%s/%s", agoProgramPath, program.Name), agoProgramObject, 0600)
		if err != nil {
			return Err, err
		}
		return Ok, nil
	} else {
		return status, nil
	}
}

// FindProgram looks for ago program objects in current directory having programName
func FindProgram(programName string) ([]os.FileInfo, error) {
	if len(programName) < 2 {
		return nil, errors.New(fmt.Sprintf("%s is too short", programName))
	}
	subDir := fmt.Sprintf("%s/%s/", ProgramDir, programName[:2])
	files, err := ioutil.ReadDir(subDir)
	if err != nil {
		return nil, errors.New(fmt.Sprintf("%s not found", programName))
	}
	found := false
	var programs []os.FileInfo
	for _, file := range files {
		if file.Name()[:len(programName)] == programName {
			found = true
			programs = append(programs, file)
		}
	}
	if !found {
		return nil, errors.New(fmt.Sprintf("%s not found", programName))
	}
	return programs, nil
}

// CheckPOSignature checks data whether is has ago program object signature
func CheckPOSignature(data []byte) ProgramStatus {
	if bytes.Equal(data[:11], []byte{0xAC, 0x04, 'A', 'G', 'O', 'S', 'Y', 'S', 'P', 'O', 0x0}) {
		return ProgramSignatureValid
	}
	return ProgramSignatureNotValid
}

// readData extracts specified portion from data
func readData(data []byte, portion ProgramPortion) (ProgramStatus, []byte) {
	if CheckPOSignature(data) == ProgramSignatureValid {
		plidx := uint32(11)
		pllim := plidx + uint32(4)
		pl := binary.LittleEndian.Uint32(data[plidx:pllim])

		pdixd := pllim + 1
		pdlim := pdixd + pl
		pd := data[pdixd:pdlim]

		polidx := pdlim + 1
		pollim := polidx + uint32(4)
		pol := binary.LittleEndian.Uint32(data[polidx:pollim])

		poidx := pollim + 1
		polim := poidx + pol
		po := data[poidx:polim]

		pelidx := polim + 1
		pellim := pelidx + uint32(4)
		pel := binary.LittleEndian.Uint32(data[pelidx:pellim])

		peidx := pellim + 1
		pelim := peidx + pel
		pe := data[peidx:pelim]

		switch portion {
		case Text:
			return Ok, pd
		case StdOut:
			return Ok, po
		case StdErr:
			return Ok, pe
		default:
			return ProgramPortionNOtValid, nil
		}
	} else {
		return ProgramSignatureNotValid, nil
	}
}
