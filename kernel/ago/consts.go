package ago

const (
	BaseDir    = ".ago/"
	ProgramDir = BaseDir + "programs/"
)

const (
	Ok                       = 0x0
	Err                      = 0x1
	ProgramTextEmpty         = 0x02
	ProgramSignatureValid    = 0x03
	ProgramSignatureNotValid = 0x04
	ProgramPortionNOtValid   = 0x05
	ProgramNotFound          = 0x06
)

const (
	Text   = 0x0
	StdOut = 0x01
	StdErr = 0x02
)
