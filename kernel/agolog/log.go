package agolog

import (
	"fmt"
	"log"
	"os"
	"os/user"
	"syscall"
)

var (
	globalLogFile string
)

func init() {
	euid := syscall.Geteuid()
	usr, _ := user.LookupId(fmt.Sprintf("%d", euid))
	globalLogFile = fmt.Sprintf("%s/.config/ago/log", usr.HomeDir)
	file, err := os.OpenFile(globalLogFile, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0666)
	if err != nil {
		fmt.Println("could not initiate logging system")
	}
	log.SetOutput(file)
}

func Log(message string) {
	log.Println(message)
}