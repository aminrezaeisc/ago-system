package config

const (
	Version = 0x01
)

const (
	Ok               = 0x0
	Err              = 0x01
	UnknownConfType  = 0x02
	ConfFileNotFound = 0x03
)

const (
	GlobalConfig  = 0x0
	RemoteConfig  = 0x01
	ServiceConfig = 0x02
)

const (
	globalConfigFile  = "config"
	remoteConfigFile  = "remote"
	serviceConfigFile = "service"
)