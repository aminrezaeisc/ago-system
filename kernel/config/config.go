package config

import (
	"fmt"
	"gopkg.in/ini.v1"
	"os"
	"os/user"
	"syscall"
)

var (
	globalConfigDir string
)

func init() {
	euid := syscall.Geteuid()
	usr, _ := user.LookupId(fmt.Sprintf("%d", euid))
	globalConfigDir = fmt.Sprintf("%s/.config/ago/", usr.HomeDir)
}

type ConfType byte
type ConfStatus byte

// Status looks for absence of config files
func Status(confType ConfType) ConfStatus {
	var configFile string
	switch confType {
	case GlobalConfig:
		configFile = fmt.Sprintf("%s/%s", globalConfigDir, globalConfigFile)
	case RemoteConfig:
		configFile = fmt.Sprintf("%s/%s", globalConfigDir, remoteConfigFile)
	case ServiceConfig:
		configFile = fmt.Sprintf("%s/%s", globalConfigDir, serviceConfigFile)
	default:
		return UnknownConfType
	}
	_, err := ini.Load(configFile)
	if err != nil {
		return ConfFileNotFound
	}
	return Ok
}

// Init initializes global config files
func Init() ConfStatus {
	if err := os.MkdirAll(fmt.Sprintf("%s/", globalConfigDir), 0755); err != nil {
		return Err
	}

	if _, err := os.OpenFile(fmt.Sprintf("%s/%s", globalConfigDir, globalConfigFile),
		os.O_CREATE | os.O_RDWR, 0664); err != nil {
		return Err
	}

	if _, err := os.OpenFile(fmt.Sprintf("%s/%s", globalConfigDir, remoteConfigFile),
		os.O_CREATE | os.O_RDWR, 0664); err != nil {
		return Err
	}

	if _, err := os.OpenFile(fmt.Sprintf("%s/%s", globalConfigDir, serviceConfigFile),
		os.O_CREATE | os.O_RDWR, 0664); err != nil {
		return Err
	}

	return Ok
}

// Fetch loads confType config file and fetches value of section-key
func Fetch(section, key string, confType ConfType) (ConfStatus, string) {
	var configFile string
	switch confType {
	case GlobalConfig:
		configFile = fmt.Sprintf("%s/%s", globalConfigDir, globalConfigFile)
	case RemoteConfig:
		configFile = fmt.Sprintf("%s/%s", globalConfigDir, remoteConfigFile)
	case ServiceConfig:
		configFile = fmt.Sprintf("%s/%s", globalConfigDir, serviceConfigFile)
	default:
		return UnknownConfType, ""
	}
	config, err := ini.Load(configFile)
	if err != nil {
		return ConfFileNotFound, ""
	}
	if config != nil {
		return Ok, config.Section(section).Key(key).String()
	}
	return Err, ""
}

// FetchAll loads confType config file and fetches values of section-repeated-key
func FetchAll(section, key string, confType ConfType) (ConfStatus, []string) {
	var configFile string
	switch confType {
	case GlobalConfig:
		configFile = fmt.Sprintf("%s/%s", globalConfigDir, globalConfigFile)
	case RemoteConfig:
		configFile = fmt.Sprintf("%s/%s", globalConfigDir, remoteConfigFile)
	case ServiceConfig:
		configFile = fmt.Sprintf("%s/%s", globalConfigDir, serviceConfigFile)
	default:
		return UnknownConfType, nil
	}
	config, err := ini.ShadowLoad(configFile)
	if err != nil {
		return ConfFileNotFound, nil
	}
	if config != nil {
		return Ok, config.Section(section).Key(key).ValueWithShadows()
	}
	return Err, nil
}

// NewSection creates a new section in confType config file
func NewSection(section string, confType ConfType) ConfStatus {
	var configFile string
	switch confType {
	case GlobalConfig:
		configFile = fmt.Sprintf("%s/%s", globalConfigDir, globalConfigFile)
	case RemoteConfig:
		configFile = fmt.Sprintf("%s/%s", globalConfigDir, remoteConfigFile)
	case ServiceConfig:
		configFile = fmt.Sprintf("%s/%s", globalConfigDir, serviceConfigFile)
	default:
		return UnknownConfType
	}
	config, err := ini.ShadowLoad(configFile)
	if err != nil {
		return ConfFileNotFound
	}
	if config != nil {
		_, err := config.NewSection(section)
		if err != nil {
			return Err
		}
		err = config.SaveTo(configFile)
		if err != nil {
			return Err
		}
		return Ok
	}
	return Err
}

// NewKey creates a new key value pair for section of confType config file
func NewKey(section, key, value string, confType ConfType) ConfStatus {
	var configFile string
	switch confType {
	case GlobalConfig:
		configFile = fmt.Sprintf("%s/%s", globalConfigDir, globalConfigFile)
	case RemoteConfig:
		configFile = fmt.Sprintf("%s/%s", globalConfigDir, remoteConfigFile)
	case ServiceConfig:
		configFile = fmt.Sprintf("%s/%s", globalConfigDir, serviceConfigFile)
	default:
		return UnknownConfType
	}
	config, err := ini.ShadowLoad(configFile)
	if err != nil {
		return ConfFileNotFound
	}
	if config != nil {
		if _, err := config.GetSection(section); err == nil {
			NewSection(section, confType)
		}
		_, err := config.Section(section).NewKey(key, value)
		if err != nil {
			return Err
		}
		err = config.SaveTo(configFile)
		if err != nil {
			return Err
		}
		return Ok
	}
	return Err
}

// Set set given value to section-key of confType config file
func Set(section, key, value string, confType ConfType) ConfStatus {
	var configFile string
	switch confType {
	case GlobalConfig:
		configFile = fmt.Sprintf("%s/%s", globalConfigDir, globalConfigFile)
	case RemoteConfig:
		configFile = fmt.Sprintf("%s/%s", globalConfigDir, remoteConfigFile)
	case ServiceConfig:
		configFile = fmt.Sprintf("%s/%s", globalConfigDir, serviceConfigFile)
	default:
		return UnknownConfType
	}
	config, err := ini.ShadowLoad(configFile)
	if err != nil {
		return ConfFileNotFound
	}
	if config != nil {
		config.Section(section).Key(key).SetValue(value)
		err = config.SaveTo(configFile)
		if err != nil {
			return Err
		}
		return Ok
	}
	return Err
}

// Delete deletes given key from confType config file, if key was repeated all keys will be deleted
func Delete(section, key string, confType ConfType) ConfStatus {
	var configFile string
	switch confType {
	case GlobalConfig:
		configFile = fmt.Sprintf("%s/%s", globalConfigDir, globalConfigFile)
	case RemoteConfig:
		configFile = fmt.Sprintf("%s/%s", globalConfigDir, remoteConfigFile)
	case ServiceConfig:
		configFile = fmt.Sprintf("%s/%s", globalConfigDir, serviceConfigFile)
	default:
		return UnknownConfType
	}
	config, err := ini.ShadowLoad(configFile)
	if err != nil {
		return ConfFileNotFound
	}
	if config != nil {
		config.Section(section).DeleteKey(key)
		err = config.SaveTo(configFile)
		if err != nil {
			return Err
		}
		return Ok
	}
	return Err
}

// Delete deletes given key that has value of given value from confType config file
func DeleteOne(section, key, value string, confType ConfType) ConfStatus {
	var configFile string
	switch confType {
	case GlobalConfig:
		configFile = fmt.Sprintf("%s/%s", globalConfigDir, globalConfigFile)
	case RemoteConfig:
		configFile = fmt.Sprintf("%s/%s", globalConfigDir, remoteConfigFile)
	case ServiceConfig:
		configFile = fmt.Sprintf("%s/%s", globalConfigDir, serviceConfigFile)
	default:
		return UnknownConfType
	}
	config, err := ini.ShadowLoad(configFile)
	if err != nil {
		return ConfFileNotFound
	}
	if config != nil {
		if config.Section(section).HasKey(key) {
			found := false
			values := config.Section(section).Key(key).ValueWithShadows()
			for _, v := range values {
				if value == v {
					found = true
					config.Section(section).DeleteKey(key)
				}
			}
			if found {
				for _, v := range values {
					if value != v {
						_, err := config.Section(section).NewKey(key, v)
						if err != nil {
							return Err
						}
					}
				}
				err = config.SaveTo(configFile)
				if err != nil {
					return Err
				}
			}
		}
		return Ok
	}
	return Err
}

// DeleteSection deletes section of confType config file
func DeleteSection(section string, confType ConfType) ConfStatus {
	var configFile string
	switch confType {
	case GlobalConfig:
		configFile = fmt.Sprintf("%s/%s", globalConfigDir, globalConfigFile)
	case RemoteConfig:
		configFile = fmt.Sprintf("%s/%s", globalConfigDir, remoteConfigFile)
	case ServiceConfig:
		configFile = fmt.Sprintf("%s/%s", globalConfigDir, serviceConfigFile)
	default:
		return UnknownConfType
	}
	config, err := ini.ShadowLoad(configFile)
	if err != nil {
		return ConfFileNotFound
	}
	if config != nil {
		config.DeleteSection(section)
		return Ok
	}
	return Err
}
