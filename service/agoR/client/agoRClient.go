package client

import (
	agoNet "agoSystem/kernel/net"
	"agoSystem/service/agoR"
	"context"
)

func Discover(location string) (agoR.Service, error) {
	conn, err := agoNet.GrpcClientConnection(location)
	if err != nil {
		return agoR.Service{}, err
	}
	remote := agoR.NewAgoRClient(conn)
	res, err := remote.Discover(context.Background(), &agoR.DiscoveryPacketReq{
		Version: 1,
	})
	if err != nil {
		return agoR.Service{}, err
	}
	return agoR.Service{
		SagoR:  res.R,
		SagoRE: res.RE,
		SagoRELocation: res.RELocation,
	}, nil
}
