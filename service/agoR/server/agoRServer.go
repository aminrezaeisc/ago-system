package main

import (
	"agoSystem/kernel/config"
	agoNet "agoSystem/kernel/net"
	"agoSystem/service/agoR"
	"fmt"
	"google.golang.org/grpc"
)

func main() {
	switch config.Status(config.ServiceConfig) {
	case config.ConfFileNotFound:
		fmt.Println("ago's not configured yet. use \"ago config init\"")
		return
	}
	status, location := config.Fetch("service agoR", "location", config.ServiceConfig)
	if status != config.Ok {
		return
	}
	listener, err := agoNet.TcpListener(location)
	if err != nil {
		fmt.Println(err)
		return
	}
	if status := config.Set("service agoR", "status", "on", config.ServiceConfig); status != config.Ok {
		fmt.Println("failed to initialize service")
		return
	}
	_, agoREStatus := config.Fetch("service agoRE", "status", config.ServiceConfig)
	SagoRE := false
	SagoRELocation := ""
	if agoREStatus == "on" {
		SagoRE = true
		status, SagoRELocation = config.Fetch("service agoRE", "location", config.ServiceConfig)
		// TODO 0.0.0.0:port -> ip:port
	}
	agoRServer := agoR.Server{Service: agoR.Service{SagoR: true, SagoRE: SagoRE, SagoRELocation: SagoRELocation}}
	grpcServer := grpc.NewServer()
	agoR.RegisterAgoRServer(grpcServer, &agoRServer)
	err = grpcServer.Serve(listener)
	if err != nil {
		fmt.Printf("failed to serve grpc server on %s: %v\n", location, err)
		config.Set("service agoR", "status", "off", config.ServiceConfig)
	}
}
