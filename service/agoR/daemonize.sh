#!/bin/bash
service="[Unit]
Description = ago system discovery service

[Service]
Type = simple
ExecStart = ${HOME}/ago/bin/rago
ExecStop = ${HOME}/ago/bin/ago config service agoR off

[Install]
WantedBy=multi-user.target
"

echo "${service}" > service/agoR/rago.service