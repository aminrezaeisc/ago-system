package agoR

import (
	"agoSystem/kernel/agolog"
	"agoSystem/kernel/config"
	"context"
	"fmt"
	"google.golang.org/grpc/peer"
)

type Service struct {
	SagoR          bool
	SagoRE         bool
	SagoRELocation string
}
type Server struct {
	Service
}

func (server *Server) Discover(ctx context.Context, req *DiscoveryPacketReq) (*DiscoveryPacketRes, error) {
	client, _ := peer.FromContext(ctx)
	agolog.Log(fmt.Sprintf("New connection from %s {\n\tversion: %d\n}", client.Addr.String(), req.Version))
	return &DiscoveryPacketRes{
		Version:    config.Version,
		R:          server.SagoR,
		RE:         server.SagoRE,
		RELocation: server.SagoRELocation,
	}, nil
}
