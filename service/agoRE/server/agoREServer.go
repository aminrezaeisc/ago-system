package main

import (
	"agoSystem/kernel/config"
	agoNet "agoSystem/kernel/net"
	"agoSystem/service/agoRE"
	"fmt"
	"google.golang.org/grpc"
)

func main() {
	switch config.Status(config.ServiceConfig) {
	case config.ConfFileNotFound:
		fmt.Println("ago's not configured yet. use \"ago config init\"")
		return
	}
	status, location := config.Fetch("service agoRE", "location", config.ServiceConfig)
	if status != config.Ok {
		fmt.Println("could not fetch agoRE service location")
		return
	}
	listener, err := agoNet.TcpListener(location)
	if err != nil {
		fmt.Println(err)
		return
	}
	if status := config.Set("service agoRE", "status", "on", config.ServiceConfig); status != config.Ok {
		fmt.Println("failed to initialize service")
		return
	}
	agoREServer := agoRE.Server{}
	grpcServer := grpc.NewServer()
	agoRE.RegisterAgoREServer(grpcServer, &agoREServer)
	err = grpcServer.Serve(listener)
	if err != nil {
		fmt.Printf("failed to serve grpc server on %s: %v\n", location, err)
		config.Set("service agoRE", "status", "off", config.ServiceConfig)
	}
}
