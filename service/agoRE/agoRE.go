package agoRE

import (
	"agoSystem/kernel/ago"
	"agoSystem/kernel/agolog"
	"context"
	"fmt"
	"google.golang.org/grpc/peer"
)

type Server struct {}

func (server *Server) Run(ctx context.Context, program *Program) (*ProgramResult, error) {
	client, _ := peer.FromContext(ctx)
	agolog.Log(fmt.Sprintf("New program from %s {\n\thash: %s\n}", client.Addr.String(), program.Hash))
	agoProgram := ago.Program{
		Name:   program.Hash,
		Text:   program.Text,
	}
	_, _ = agoProgram.Run()
	return &ProgramResult{
		Hash:          agoProgram.Name,
		StdOutput:     agoProgram.StdOut,
		StdErr:        agoProgram.StdErr,
	}, nil
}