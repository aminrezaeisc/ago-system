#!/bin/bash
service="[Unit]
Description = ago system remote program execution service

[Service]
Type = simple
ExecStart = ${HOME}/ago/bin/reago
ExecStartPost = systemctl restart rago.service
ExecStop = ${HOME}/ago/bin/ago config service agoRE off
ExecStopPost = systemctl restart rago.service

[Install]
WantedBy=multi-user.target
"

echo "${service}" > service/agoRE/reago.service