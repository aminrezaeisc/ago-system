package client

import (
	"agoSystem/kernel/ago"
	"agoSystem/kernel/agolog"
	agoNet "agoSystem/kernel/net"
	"agoSystem/service/agoRE"
	"context"
)

func RemoteRun(program *ago.Program, location string) {
	conn, err := agoNet.GrpcClientConnection(location)
	if err != nil {
		agolog.Log(err.Error())
	}
	remote := agoRE.NewAgoREClient(conn)
	res, err := remote.Run(context.Background(), &agoRE.Program{
		Hash: program.Name,
		Text: program.Text,
	})
	if err != nil {
		agolog.Log(err.Error())
	}
	// TODO specify program status whether it's doing ok
	if res != nil {
		program.StdOut = res.StdOutput
		program.StdErr = res.StdErr
	}
}
