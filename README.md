agoSystem

1. Brief description:  
    It's all about distributing programs as many as there are machines to work on those.
By distributing and managing programs no one will worry about his/her computing resources as there are a lot of them.
agoSys would connect you to people who use agoSys for using their resources as long as your computers connected together.

2. About project:  
    This project is a university thesis, so it's not going to be useful for commercial works.
  
3. Project Architecture:
> This section is incomplete. 

4. Prerequisites:
    1. [Go](https://golang.org/doc/install)
    2. __make__
        * Ubuntu: apt install make
        * CentOS: yum install make
    3. __systemd__
        * Ubuntu: apt install systemd
        * CentOS 7+
    4. [Protocol buffer](https://developers.google.com/protocol-buffers) compiler (protoc) version 3
        * Ubuntu: apt install protobuf-compiler
    5. __Go plugin__ for the protocol compiler  
        * Install the protocol compiler plugin for Go (protoc-gen-go):
            ```bash
            $ export GO111MODULE=on  # Enable module mode
            $ go get github.com/golang/protobuf/protoc-gen-go@v1.4
            ```
       * Update your PATH so that the protoc compiler can find the plugin:
           ```bash
           $ export PATH="$PATH:$(go env GOPATH)/bin"
           ```
5. Installation:
    1. Clone the project or download the source code and extract it
    2. Build the project
        ```bash
        $ cd ago-system
        $ make
        $ make proto
        $ make configure
        $ make daemon
        $ sudo make install
        ```
    3. Update your PATH:
        ```bash
        $ export PATH="$PATH:$HOME/ago/bin"
        ```
       > add it to your shell configuration file, e.g. $HOME/.bashrc or more globally in /etc/environment.
         exit the terminal or create a new one and in a server logout the session and login to reflect these changes.                                                                                              
    4. Start ago discovery services
        ```bash
        $ sudo systemctl start rago.service 
       ```
    5. Start ago remote execution services
        ```bash
        $ sudo systemctl start reago.service
        ```